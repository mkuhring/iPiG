/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.rki.ng4.ipig.Ipig;
import de.rki.ng4.ipig.exceptions.ExitException;
import de.rki.ng4.ipig.tools.Configurator;
import java.awt.Toolkit;


/**
 * <p>This is a GUI class for iPiG. It offers fields and buttons for necessary file indications
 *  and options.</p> 
 * 
 * <p>The start button will create a new thread which runs the iPiG main routine 
 * while the iPiG output is redirected to a text area.</p>
 * 
 * @author Mathias Kuhring
 *
 */
public class IpigGui {
	private JFrame frmipigGui;
	private JTextField tf_geneAnnoFile;
	private JButton btn_geneAnnoFile;
	private JLabel lbl_geneAnnoFile;
	private JTextField tf_geneAaSeqFile;
	private JButton btn_geneAaSeqFile;
	private JLabel lbl_geneAaSeqFile;
	private JTextField tf_protMapFile;
	private JButton btn_protMapFile;
	private JLabel lbl_protMapFile;
	private JTextField tf_protSeqFile;
	private JButton btn_protSeqFile;
	private JLabel lbl_protSeqFile;
	private JButton btn_start;
	private OutputJTextArea outputWindow;
	private Thread mapper;
	private JButton btn_stop;
	private JTextField tf_msPepFile;
	private JButton btn_msPepFile;
	private JLabel lbl_msPepFile;
	private JPanel input;
	private JPanel resources;
	private JPanel settings;
	private JPanel panel_button;
	private JPanel panel_geneAnnoFile;
	private JPanel panel_geneAaSeqFile;
	private JPanel panel_protMapFile;
	private JPanel panel_protSeqFile;
	private JPanel panel_msPepFile;
	private JLabel lblScoreRange;
	private JSpinner sp_minScore;
	private JLabel lblTo;
	private JSpinner sp_maxScore;
	private JLabel lblThresholds;
	private JSpinner sp_threshold1;
	private JLabel lblAnd;
	private JSpinner sp_threshold2;
	private JPanel panel_11;
	private JPanel panel_9;
	private JLabel lblColors;
	private JTextField color1;
	private JTextField color2;
	private JTextField color3;
	private JPanel panel_10;
	private JPanel panel_12;
	private JPanel execution;
	private JPanel config;
	private JPanel panel_1;
	private JButton btn_load;
	private JButton btn_save;
	private JPanel menu;
	private JPanel panel_3;
	private JButton btn_help;
	private JButton btn_exit;
	private JPanel panel_outputPath;
	private JTextField tf_outputPath;
	private JButton btn_outputPath;
	private JLabel lbl_outputPath;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IpigGui window = new IpigGui();
					window.frmipigGui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IpigGui() {
		initialize();
		updateFields();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// try native look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			System.out.println("Error setting native LAF: " + e);
		}

		frmipigGui = new JFrame();
		frmipigGui.setIconImage(Toolkit.getDefaultToolkit().getImage(IpigGui.class.getResource("/images/rki.png")));
		frmipigGui.setResizable(false);
		frmipigGui.setTitle("iPiG GUI");
		frmipigGui.setBounds(100, 100, 500, 600);
		frmipigGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmipigGui.getContentPane().setLayout(null);

		input = new JPanel();
		input.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Input", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 70, 213)));
		input.setBounds(10, 10, 473, 100);
		frmipigGui.getContentPane().add(input);
		input.setLayout(null);

		panel_msPepFile = new JPanel();
		panel_msPepFile.setBounds(10, 14, 455, 36);
		input.add(panel_msPepFile);
		panel_msPepFile.setLayout(null);

		tf_msPepFile = new JTextField();
		tf_msPepFile.setBounds(0, 14, 356, 20);
		panel_msPepFile.add(tf_msPepFile);
		tf_msPepFile.setEditable(false);
		tf_msPepFile.setText("");
		tf_msPepFile.setColumns(10);

		btn_msPepFile = new JButton("Open");
		btn_msPepFile.setBounds(366, 13, 89, 23);
		btn_msPepFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(tf_msPepFile.getText()), new FileNameExtensionFilter("Peptides (txt, mzid)", "txt", "mzid"));
				if (file != null){
					Configurator.setSysProperty("msPepFile", file.getAbsolutePath());
					Configurator.setSysProperty("msPepSetName", file.getName().substring(0,file.getName().lastIndexOf(".")));
					updateFields();
				}
			}
		});
		panel_msPepFile.add(btn_msPepFile);

		lbl_msPepFile = new JLabel("Peptide Spectrum Matches:");
		lbl_msPepFile.setBounds(0, 0, 374, 14);
		panel_msPepFile.add(lbl_msPepFile);

		panel_outputPath = new JPanel();
		panel_outputPath.setLayout(null);
		panel_outputPath.setBounds(10, 54, 455, 36);
		input.add(panel_outputPath);

		tf_outputPath = new JTextField();
		tf_outputPath.setText("");
		tf_outputPath.setEditable(false);
		tf_outputPath.setColumns(10);
		tf_outputPath.setBounds(0, 14, 356, 20);
		panel_outputPath.add(tf_outputPath);

		btn_outputPath = new JButton("Open");
		btn_outputPath.setBounds(366, 13, 89, 23);
		btn_outputPath.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = choosePath(new File(tf_outputPath.getText()));
				if (file != null){
					Configurator.setProperty("outputPath",file.getAbsolutePath());
					updateFields();
				}
			}
		});
		panel_outputPath.add(btn_outputPath);

		lbl_outputPath = new JLabel("Output Path (optional):");
		lbl_outputPath.setBounds(0, 0, 374, 14);

		panel_outputPath.add(lbl_outputPath);

		resources = new JPanel();
		resources.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Resources", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 70, 213)));
		resources.setBounds(10, 110, 473, 197);
		frmipigGui.getContentPane().add(resources);
		resources.setLayout(null);

		panel_geneAnnoFile = new JPanel();
		panel_geneAnnoFile.setBounds(10, 14, 455, 36);
		resources.add(panel_geneAnnoFile);
		panel_geneAnnoFile.setLayout(null);

		tf_geneAnnoFile = new JTextField();
		tf_geneAnnoFile.setBounds(0, 14, 356, 20);
		panel_geneAnnoFile.add(tf_geneAnnoFile);
		tf_geneAnnoFile.setEditable(false);
		tf_geneAnnoFile.setColumns(10);

		btn_geneAnnoFile = new JButton("Open");
		btn_geneAnnoFile.setBounds(366, 13, 89, 23);
		panel_geneAnnoFile.add(btn_geneAnnoFile);

		lbl_geneAnnoFile = new JLabel("UCSC/Ensembl Genes table file:");
		lbl_geneAnnoFile.setBounds(0, 0, 374, 14);
		panel_geneAnnoFile.add(lbl_geneAnnoFile);
		btn_geneAnnoFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(tf_geneAnnoFile.getText()), new FileNameExtensionFilter("Text file", "txt"));
				if (file != null){
					Configurator.setProperty("geneAnnoFile",file.getAbsolutePath());
					updateFields();
				}
			}
		});

		panel_geneAaSeqFile = new JPanel();
		panel_geneAaSeqFile.setBounds(10, 59, 455, 36);
		resources.add(panel_geneAaSeqFile);
		panel_geneAaSeqFile.setLayout(null);

		tf_geneAaSeqFile = new JTextField();
		tf_geneAaSeqFile.setBounds(0, 14, 356, 20);
		panel_geneAaSeqFile.add(tf_geneAaSeqFile);
		tf_geneAaSeqFile.setEditable(false);
		tf_geneAaSeqFile.setColumns(10);

		btn_geneAaSeqFile = new JButton("Open");
		btn_geneAaSeqFile.setBounds(366, 13, 89, 23);
		btn_geneAaSeqFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(tf_geneAaSeqFile.getText()), new FileNameExtensionFilter("Text file", "txt"));
				if (file != null){
					Configurator.setProperty("geneAaSeqFile",file.getAbsolutePath());
					updateFields();
				}
			}
		});
		panel_geneAaSeqFile.add(btn_geneAaSeqFile);

		lbl_geneAaSeqFile = new JLabel("UCSC/Ensembl Amino Acid Sequences table file:");
		lbl_geneAaSeqFile.setBounds(0, 0, 374, 14);
		panel_geneAaSeqFile.add(lbl_geneAaSeqFile);

		panel_protMapFile = new JPanel();
		panel_protMapFile.setBounds(10, 106, 455, 36);
		panel_protMapFile.setLayout(null);
		resources.add(panel_protMapFile);

		tf_protMapFile = new JTextField();
		tf_protMapFile.setBounds(0, 14, 356, 20);
		panel_protMapFile.add(tf_protMapFile);
		tf_protMapFile.setEditable(false);
		tf_protMapFile.setColumns(10);

		btn_protMapFile = new JButton("Open");
		btn_protMapFile.setBounds(366, 13, 89, 23);
		panel_protMapFile.add(btn_protMapFile);

		lbl_protMapFile = new JLabel("UniProt ID-mapping file (optional but recommended):");
		lbl_protMapFile.setBounds(0, 0, 374, 14);
		panel_protMapFile.add(lbl_protMapFile);
		btn_protMapFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(tf_protMapFile.getText()), new FileNameExtensionFilter("Tab file", "tab" ));
				if (file != null){
					Configurator.setProperty("protMapFile",file.getAbsolutePath());
					updateFields();
				}
			}
		});

		panel_protSeqFile = new JPanel();
		panel_protSeqFile.setBounds(10, 151, 455, 36);
		resources.add(panel_protSeqFile);
		panel_protSeqFile.setLayout(null);

		tf_protSeqFile = new JTextField();
		tf_protSeqFile.setBounds(0, 14, 356, 20);
		panel_protSeqFile.add(tf_protSeqFile);
		tf_protSeqFile.setEditable(false);
		tf_protSeqFile.setColumns(10);

		btn_protSeqFile = new JButton("Open");
		btn_protSeqFile.setBounds(366, 13, 89, 23);
		panel_protSeqFile.add(btn_protSeqFile);

		lbl_protSeqFile = new JLabel("Proteome fasta file (optional):");
		lbl_protSeqFile.setBounds(0, 0, 374, 14);
		panel_protSeqFile.add(lbl_protSeqFile);

		btn_protSeqFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(tf_protSeqFile.getText()), new FileNameExtensionFilter("Fasta file", "fasta"));
				if (file != null){
					Configurator.setProperty("protSeqFile",file.getAbsolutePath());
					updateFields();
				}
			}
		});

		settings = new JPanel();
		settings.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Export Settings", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 70, 213)));
		settings.setBounds(10, 307, 473, 68);
		frmipigGui.getContentPane().add(settings);
		settings.setLayout(null);

		panel_12 = new JPanel();
		panel_12.setBounds(10, 16, 453, 20);
		settings.add(panel_12);
		panel_12.setLayout(null);

		panel_11 = new JPanel();
		panel_11.setBounds(0, 0, 218, 20);
		panel_12.add(panel_11);
		panel_11.setLayout(null);

		lblScoreRange = new JLabel("Score range:");
		lblScoreRange.setBounds(0, 3, 62, 14);
		panel_11.add(lblScoreRange);


		sp_minScore = new JSpinner();
		sp_minScore.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Configurator.setProperty("minScore", sp_minScore.getValue().toString());
			}
		});
		sp_minScore.setBounds(72, 0, 63, 20);
		panel_11.add(sp_minScore);

		lblTo = new JLabel("-");
		lblTo.setBounds(145, 3, 4, 14);
		panel_11.add(lblTo);
		lblTo.setHorizontalAlignment(SwingConstants.CENTER);

		sp_maxScore = new JSpinner();
		sp_maxScore.setBounds(155, 0, 63, 20);
		sp_maxScore.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Configurator.setProperty("maxScore", sp_maxScore.getValue().toString());
			}
		});
		panel_11.add(sp_maxScore);

		panel_9 = new JPanel();
		panel_9.setBounds(225, 0, 228, 20);
		panel_12.add(panel_9);
		panel_9.setLayout(null);

		lblThresholds = new JLabel("Thresholds:");
		lblThresholds.setBounds(6, 3, 56, 14);
		panel_9.add(lblThresholds);

		sp_threshold1 = new JSpinner();
		sp_threshold1.setBounds(72, 0, 63, 20);
		sp_threshold1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Configurator.setProperty("threshold1", sp_threshold1.getValue().toString());
			}
		});
		panel_9.add(sp_threshold1);

		lblAnd = new JLabel("&");
		lblAnd.setBounds(145, 3, 7, 14);
		panel_9.add(lblAnd);
		lblAnd.setHorizontalAlignment(SwingConstants.CENTER);

		sp_threshold2 = new JSpinner();
		sp_threshold2.setBounds(165, 0, 63, 20);
		sp_threshold2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Configurator.setProperty("threshold2", sp_threshold2.getValue().toString());
			}
		});
		panel_9.add(sp_threshold2);

		panel_10 = new JPanel();
		panel_10.setBounds(74, 40, 324, 20);
		settings.add(panel_10);
		panel_10.setLayout(null);

		lblColors = new JLabel("Colors:");
		lblColors.setBounds(0, 3, 34, 14);
		panel_10.add(lblColors);

		color1 = new JTextField();
		color1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				Configurator.setProperty("color1", color1.getText());
			}
		});
		color1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configurator.setProperty("color1", color1.getText());
			}
		});
		color1.setBounds(46, 0, 86, 20);
		panel_10.add(color1);
		color1.setColumns(10);

		color2 = new JTextField();
		color2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				Configurator.setProperty("color2", color2.getText());
			}
		});
		color2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configurator.setProperty("color2", color2.getText());
			}
		});
		color2.setBounds(142, 0, 86, 20);
		panel_10.add(color2);
		color2.setColumns(10);

		color3 = new JTextField();
		color3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				Configurator.setProperty("color3", color2.getText());
			}
		});
		color3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configurator.setProperty("color3", color3.getText());
			}
		});
		color3.setBounds(238, 0, 86, 20);
		panel_10.add(color3);
		color3.setColumns(10);

		execution = new JPanel();
		execution.setBounds(10, 375, 147, 44);
		frmipigGui.getContentPane().add(execution);
		execution.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Execution", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 70, 213)));
		execution.setLayout(null);

		panel_button = new JPanel();
		panel_button.setBounds(6, 14, 135, 23);
		execution.add(panel_button);
		panel_button.setLayout(null);

		// start button creates and starts a mapper thread
		// parameters are the checked in ipig
		btn_start = new JButton("Start");
		btn_start.setBounds(0, 0, 65, 23);
		panel_button.add(btn_start);
		btn_start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mapper = new Mapper();
				mapper.start();
			}
		});

		// stop button sets a breakpoint, which is checked by the ipig periodically.
		// if the mapper thread is already dead (e.g. crashed), the button restores the interface by enabling the other buttons itself.
		btn_stop = new JButton("Stop");
		btn_stop.setEnabled(false);
		btn_stop.setBounds(70, 0, 65, 23);
		panel_button.add(btn_stop);
		btn_stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (mapper != null && mapper.isAlive()){
					Configurator.setBreak(true);
					btn_stop.setEnabled(false);
				}
				else{
					enableButtons(true);
					Configurator.setBreak(false);
					outputWindow.redirectSystemStreams(false, false);
				}	
			}
		});

		config = new JPanel();
		config.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Configuration", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 70, 213)));
		config.setLayout(null);
		config.setBounds(173, 375, 147, 44);
		frmipigGui.getContentPane().add(config);

		panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setOpaque(false);
		panel_1.setBounds(6, 14, 135, 23);
		config.add(panel_1);

		// config load button
		btn_load = new JButton("Load");
		btn_load.setBounds(0, 0, 65, 23);
		btn_load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = chooseFile(new File(""), new FileNameExtensionFilter("configuration file", "conf", "txt"));
				if (file != null){
					try {
						Configurator.loadProperties(file.getAbsolutePath());
						updateFields();
						outputWindow.setText("configuration loaded");
					} catch (ExitException e) {
						outputWindow.setText("error: couldn't load config (" + e.getMessage() + ")");
					}
				}
			}
		});
		panel_1.add(btn_load);

		// config save button
		btn_save = new JButton("Save");
		btn_save.setBounds(70, 0, 65, 23);
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = saveFile(new File(""), new FileNameExtensionFilter("configuration file", "conf", "txt"));
				if (file != null){
					try {
						Configurator.saveProperties(file.getAbsolutePath());
						outputWindow.setText("configuration saved");
					} catch (ExitException e) {
						outputWindow.setText("error: couldn't save config (" + e.getMessage() + ")");
					}
				}
			}
		});
		panel_1.add(btn_save);

		menu = new JPanel();
		menu.setBorder(new TitledBorder(null, "Menu", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		menu.setLayout(null);
		menu.setBounds(336, 375, 147, 44);
		frmipigGui.getContentPane().add(menu);

		panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setOpaque(false);
		panel_3.setBounds(6, 14, 135, 23);
		menu.add(panel_3);

		// help button, loads a help text from the resources to the output window
		btn_help = new JButton("Help");
		btn_help.setBounds(0, 0, 65, 23);
		btn_help.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String help = "/help/ipiggui_help.txt";
					InputStream is = Ipig.class.getResourceAsStream(help);
					BufferedReader helpbf = new BufferedReader(new InputStreamReader(is));
					outputWindow.setText("");
					while (helpbf.ready()){
						outputWindow.append(helpbf.readLine() + "\n");
					}
					outputWindow.setCaretPosition(0); 
					is.close();
				} catch (IOException e) {
					outputWindow.append("error: " + e.getLocalizedMessage());
				}
			}
		});
		panel_3.add(btn_help);

		// exit button
		btn_exit = new JButton("Exit");
		btn_exit.setBounds(70, 0, 65, 23);
		btn_exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		panel_3.add(btn_exit);

		// the output window displays the progress or errors.
		// OutputJTextArea is a extended JTextArea which catches and displays the System.out stream,
		// so finally the output from the ipig.
		outputWindow = new OutputJTextArea();
		outputWindow.setEditable(false);
		outputWindow.setSize(473, 157);
		outputWindow.setLocation(10, 400);
		outputWindow.setLineWrap(true);

		JScrollPane scrollPane = new JScrollPane(outputWindow);
		scrollPane.setBounds(10, 424, 473, 133);
		frmipigGui.getContentPane().add(scrollPane);
	}

	// a thread running the ipig and controlling the output and buttons meanwhile
	class Mapper extends Thread{
		@Override public void run(){
			Configurator.setBreak(false);
			enableButtons(false);
			outputWindow.setText("");
			outputWindow.redirectSystemStreams(true, true);

			try {
				Ipig.run();
			} catch (ExitException e) {
				System.out.println("\n" + e.getMessage());
			}

			System.gc();
			enableButtons(true);
			Configurator.setBreak(false);
			outputWindow.redirectSystemStreams(false, false);
		}
	}

	// enabling/disabling of buttons for running the mapper
	// stop button is always set contradictory
	private void enableButtons(boolean b){
		btn_msPepFile.setEnabled(b);
		btn_geneAaSeqFile.setEnabled(b);
		btn_geneAnnoFile.setEnabled(b);
		btn_protMapFile.setEnabled(b);
		btn_protSeqFile.setEnabled(b);

		btn_start.setEnabled(b);
		btn_stop.setEnabled(!b); // not b!		
		btn_load.setEnabled(b);
		btn_save.setEnabled(b);
		btn_help.setEnabled(b);
		btn_exit.setEnabled(b);

		btn_outputPath.setEnabled(b);

		settings.setEnabled(b);
	}

	// updates the filename and parameter fields after selecting a file/folder or loading a configuration file
	private void updateFields(){
		tf_geneAnnoFile.setText(Configurator.getProperty("geneAnnoFile", ""));
		tf_geneAaSeqFile.setText(Configurator.getProperty("geneAaSeqFile", ""));
		tf_protMapFile.setText(Configurator.getProperty("protMapFile", ""));
		tf_protSeqFile.setText(Configurator.getProperty("protSeqFile", ""));
		tf_msPepFile.setText(Configurator.getSysProperty("msPepFile", ""));

		tf_outputPath.setText(Configurator.getProperty("outputPath", ""));

		try{
			sp_minScore.setValue(Integer.parseInt(Configurator.getProperty("minScore", "0")));
		}
		catch (NumberFormatException e){
			outputWindow.setText("error: couldn't parse \"minScore\" from config file \n" +
					"(" + e.getMessage() + ") - set parameter manually!");
		}
		try{
			sp_maxScore.setValue(Integer.parseInt(Configurator.getProperty("maxScore", "0")));
		}
		catch (NumberFormatException e){
			outputWindow.setText("error: couldn't parse \"maxScore\" from config file \n" +
					"(" + e.getMessage() + ") - set parameter manually!");
		}
		try{
			sp_threshold1.setValue(Integer.parseInt(Configurator.getProperty("threshold1", "0")));
		}
		catch (NumberFormatException e){
			outputWindow.setText("error: couldn't parse \"threshold1\" from config file \n" +
					"(" + e.getMessage() + ") - set parameter manually!");
		}
		try{
			sp_threshold2.setValue(Integer.parseInt(Configurator.getProperty("threshold2", "0")));
		}
		catch (NumberFormatException e){
			outputWindow.setText("error: couldn't parse \"threshold2\" from config file \n" +
					"(" + e.getMessage() + ") - set parameter manually!");
		}

		color1.setText(Configurator.getProperty("color1", ""));
		color2.setText(Configurator.getProperty("color2", ""));
		color3.setText(Configurator.getProperty("color3", ""));
	}

	// creation of a FileChooser for the file selection/open buttons
	private File chooseFile(File dir, FileNameExtensionFilter filter){
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		fc.setCurrentDirectory(dir);

		int state = fc.showOpenDialog( null );

		if ( state == JFileChooser.APPROVE_OPTION )
		{
			return fc.getSelectedFile();
		}
		else
			return null;
	}

	// creation of a FileChooser for the config save button
	private File saveFile(File dir, FileNameExtensionFilter filter){
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(filter);
		fc.setCurrentDirectory(dir);
		fc.setSelectedFile( new File("mapper.conf") );

		while ( fc.showSaveDialog( null ) == JFileChooser.APPROVE_OPTION ){
			File file = fc.getSelectedFile();
			if (file.exists())  {  
				int answer = JOptionPane.showConfirmDialog(null, "Replace existing file?");  
				if (answer == JOptionPane.OK_OPTION)  
					return fc.getSelectedFile();
				if (answer == JOptionPane.CANCEL_OPTION)
					break;
			} 
			else
				return fc.getSelectedFile();
		}
		return null;
	}

	// creation of a FileChooser for the path selection buttons
	private File choosePath(File dir){
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(dir);
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int state = fc.showOpenDialog( null );

		if ( state == JFileChooser.APPROVE_OPTION )
		{
			return fc.getSelectedFile();
		}
		else
			return null;
	}
}
