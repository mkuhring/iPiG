/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.exceptions;

/**
 * A simple exception used to exit the program. It's usually thrown up to the main class.
 * 
 * @author Mathias Kuhring
 */
public class ExitException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExitException(String string) {
		super(string);
	}

}
