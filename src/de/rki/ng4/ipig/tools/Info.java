/* Copyright (c) 2012,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch-Institut, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.ipig.tools;

/**
 * <p>Simple class to print some status messages.</p>
 * 
 * <p>There are two kinds of information contents (info modes) available (see {@link #setInfomode(boolean)}).</p>
 * 
 * @author Mathias Kuhring
 *
 */
public class Info {

	private static boolean infomode = false;

	private long begin;
	private long end;
	private static final String space = "\t";

	/**
	 * <p>An Info object is initiated with a directly printed start message followed by "...".</p>
	 * 
	 * @param message the start message
	 */
	public Info(String message){
		start(message);
	}

	// print the start message and takes the time
	private void start(String message){
		System.out.print(message + "..." + space);
		begin = System.currentTimeMillis();
	}

	/**
	 * <p>Prints either a simple "done" message or an arbitrary self-indicated ratio and the runtime, if the info mode is on ({@link #setInfomode(boolean)}).
	 * 
	 * <p>E.g. "processed 30 of 50 in 5 sec"</p>
	 * 
	 * @param part first part of the ratio
	 * @param all second part of the ratio
	 */
	public void stop(int part, int all){
		end = System.currentTimeMillis();
		if (infomode){
			System.out.println("processed " + part + " of " + all + " in " + (end-begin)/1000 + " s");
		}
		else{
			System.out.println("done");
		}
	}
	
	/**
	 * <p>Prints either a simple "done" message or the runtime, if the info mode is on (see {@link #setInfomode(boolean)}).</p>
	 * 
	 * <p>E.g. "done in 5 sec"</p>
	 */
	public void stop(){
		end = System.currentTimeMillis();
		if (infomode){
			System.out.println("done" + " in " + (end-begin)/1000 + " s");
		}
		else{
			System.out.println("done");
		}
	}

	/**
	 * <p>Switches the info mode.</p>
	 * 
	 * <p>If the info mode is off (false), an Info object only provides simple procedure start and stop messages.</p>
	 * 
	 * <p>If the info mode is on (true), an Info object additionally provides the procedure's runtime in seconds
	 * and if wanted an arbitrary self-indicated ratio.</p>
	 * 
	 * @param mode true for info mode on and false for info mode off
	 */
	public static void setInfomode(boolean mode){
		infomode = mode;
	}
}
